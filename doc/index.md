<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

The introduction to CoVeriTeam can be divided in the following parts:
- [Installation](../README.md)
- Tool usage - described below
- [Language description](language.md)
- [Tutorial](../examples/README.md)
- Actor Definitions - described below

## Tool Usage
Run `bin/coveriteam --help` to see available command line arguments. 
The values required by the CoVeriTeam program are passed using the `--input` parameter followed by 
a `key=value` pair and the CoVeriTeam program file.

```bash
bin/coveriteam
    --input key1=val1                     pass input parameters to the program
    --input key2=val2                     ... and so on
    --input key3+=val3                    Appends the value to previous defined values with key3 
    --gen-code                            print the generated python code instead of executing
    --debug                               print debug (and higher) messages from CoVeriTeam.
    --debug-deep                          print debug (and higher) messages from CoVeriTeam and also from dependent components
    <program_file>                        CoVeriTest program
    --cache-dir <CACHE_DIR>               Explicitly set the path to the cache.
    --remote                              Execute CoVeriTeam remotely.
    --tool-info                           Test the given YML file configuration by executing the tool for version.
    --only-install-actors                 Install all external tools from the given <program_file> into the <CACHE_DIR>.
    --clean                               Delete cache before execution.
    --no-cache-update                     Do not update the cache. Only use tools available in the cache. If tools are not available, then CoVeriTeam simply fails.
    --use-mpi-for-portfolio               Use MPI to execute all portfolio compositiions. This might need some setup inside the mpi-execution module.
    --allow-cgroup-access-to-actors       Allows each atomic actor access to /sys/fs/cgroups. Shouldn't be relevant for most users.
```

On successful execution a uniquely named folder is created in the folder `cvt-output`.
This folder contains 1) the artifacts produced during the execution of the actor, 
and 2) an xml file named `execution_trace.xml` containing the execution trace of the composition,
i.e., the resource measurements for atomic actors and paths to the artifacts produced.

In addition to this, a symbolic link named `lastexecution` pointing to the directory containing the artifacts
produced in the latest execution is also created.

### Atomic Actors
CoVeriTeam is based on using off-the-shelf verification and testing tools for cooperative verification
as atomic actors.
On its first execution, CoVeriTeam automatically downloads and unzips the archives that contain
the atomic actors used in the CoVeriTeam program.
The source URL is specified in the YAML atomic-actor definition.
The atomic actors that we use in the tutorial examples are available in the [actors/](actors/) folder.

`bin/coveriteam --tool-info <YAML actor-definition file>`
prints information (including name and version) about the actor defined in the YAML file.
This can be used to test if the atomic actor was successfully installed.
The script `smoke_test_all_tools.sh` prints the tool info for each actor defined in the `actors/` folder.

## Actor Definition
An actor definition file contains the information required for executing an actor.
This information includes, where to download the tool from, which version of the tool to execute, 
how to assemble its command line, etc.
The actor definition file contains this information in YAML format. 
Below we describe the fields of such a file.

```yaml
imports: 
  - !include <string> # Import another actor defintion file
actor_name: <string> # Name of the actor
toolinfo_module: <string> # Tool-info module. This can either be a url of the name of the tool-info module.
                          # This is used to assemble the command to the tool.
options: <[string]> # A list of options used to create the command line. 
archives: # A list of different versions of the tool.
  - version: <string> # Version
    location: <string> # URL of the self contained tool archive.
    spdx_license_identifier: <string> # An optional key to specify the license of the archive.
  - version: <string> # Version
    location: <string> # URL of the self contained tool archive.
  - version: <string> # Version
    location: <string> # URL of the self contained tool archive.
    options: <[string]> # Options can also be added here. These will overwrite the options specified above.
  # Possibly few more versions.
  ...
resourcelimits: # Resource limits used for tool execution.
    memlimit: # Memory limit. Allowed values "NUM UNIT", where NUM is a natural number,
              # and UNIT is "B", "kB", "MB", "GB", or "TB".
    timelimit: # Time limit. Allowed values are "NUM UNIT", where NUM is a natural number,
               # and UNIT is "s" for second, "min" for minute, "h" for hours, and "d" for days.
    cpuCores: # Number of cores.
format_version: <string> # Actor definition format version.
```